# PowerShell Script

gradlew clean build
cd mongo-import/
Start-BitsTransfer -Source https://static.openfoodfacts.org/data/openfoodfacts-mongodbdump.tar.gz -Destination openfoodfacts-mongodbdump.tar.gz
tar -xzvf openfoodfacts-mongodbdump.tar.gz
rm openfoodfacts-mongodbdump.tar.gz
mongo --eval "db.dropDatabase()" knowyourmeal
mongorestore -d knowyourmeal -c products dump/off/products.bson
rm -r dump/
java -Xms512m -Xmx1536m -jar build/libs/know-your-meal-1.0.0-SNAPSHOT.jar
dir
