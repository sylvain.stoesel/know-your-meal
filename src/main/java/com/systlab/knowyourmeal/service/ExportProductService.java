package com.systlab.knowyourmeal.service;

import com.systlab.knowyourmeal.entity.Product;
import com.systlab.knowyourmeal.utils.ExportUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;

@Service
public class ExportProductService extends AbstractExportExcelService<Product> {

  private int rowNum;

  @Override
  Class<Product> getEntityType() {
    return Product.class;
  }

  @Override
  String getFileName() {
    return "All-Products.xlsx";
  }

  @Override
  String[] getColumnsNames() {
    return new String[] {
      "ID DB",
      "Code",
      "Name",
      "Other name(s)",
      "# ingredients",
      "VEGAN",
      "VEGETARIAN",
      "GLUTEN",
      "BEEF",
      "PORK",
      "FISH",
      "ALCOHOL"
    };
  }

  @Override
  void createExcelFile() {
    super.createExcelFile();
    Cell cell = this.sheet.getRow(0).createCell(getColumnsNames().length);
    cell.setCellValue("AJOUTER ICI LE OU LES NOMS PAR LESQUELS CET ARTICLE DOIT ETRE REMPLACE");
    this.rowNum = 1;
  }

  @Override
  public void writeRowInExcelFile(Product product) {
    Row row = sheet.createRow(rowNum++);

    row.createCell(0).setCellValue(product.getId());
    row.createCell(1).setCellValue(product.getCode());
    row.createCell(2).setCellValue(product.getName());
    row.createCell(3)
        .setCellValue(
            product.getOtherNames() != null ? String.join(", ", product.getOtherNames()) : "");

    row.createCell(4)
        .setCellValue(
            product.getListIdIngredients() != null
                ? String.valueOf(product.getListIdIngredients().size())
                : "");

    row.createCell(5).setCellValue(ExportUtils.getTextForBoolean(product.getVegan()));
    row.createCell(6).setCellValue(ExportUtils.getTextForBoolean(product.getVegetarian()));
    row.createCell(7).setCellValue(ExportUtils.getTextForBoolean(product.getGluten()));
    row.createCell(8).setCellValue(ExportUtils.getTextForBoolean(product.getBeef()));
    row.createCell(9).setCellValue(ExportUtils.getTextForBoolean(product.getPork()));
    row.createCell(10).setCellValue(ExportUtils.getTextForBoolean(product.getFish()));
    row.createCell(11).setCellValue(ExportUtils.getTextForBoolean(product.getAlcohol()));
  }
}
