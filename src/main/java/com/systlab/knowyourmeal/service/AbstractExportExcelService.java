package com.systlab.knowyourmeal.service;

import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;

public abstract class AbstractExportExcelService<T> {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExportExcelService.class);

  @Autowired private MongoTemplate mongoTemplate;

  SXSSFWorkbook workbook;
  SXSSFSheet sheet;
  CellStyle styleFontSmall;

  abstract Class<T> getEntityType();

  abstract String getFileName();

  abstract String[] getColumnsNames();

  abstract void writeRowInExcelFile(T entity);

  void createExcelFile() {
    // Create a Workbook
    this.workbook = new SXSSFWorkbook(1000);

    // Create a Sheet

    this.sheet = this.workbook.createSheet(getEntityType().getSimpleName());
    // Create Font and Style
    Font fontSmall = this.workbook.getXSSFWorkbook().createFont();
    fontSmall.setFontHeightInPoints((short) 8);
    fontSmall.setFontName("Calibri");
    this.styleFontSmall = workbook.getXSSFWorkbook().createCellStyle();
    this.styleFontSmall.setFont(fontSmall);

    // Create a Row
    Row headerRow = this.sheet.createRow(0);

    // Create cells
    for (int i = 0; i < getColumnsNames().length; i++) {
      Cell cell = headerRow.createCell(i);
      cell.setCellValue(getColumnsNames()[i]);
    }
  }

  public void exportAllEntities() throws IOException {
    CloseableIterator<T> allEntities = mongoTemplate.stream(new Query(), getEntityType());

    createExcelFile();

    int nbEntitiesProcessed = 0;
    while (allEntities.hasNext()) {
      writeRowInExcelFile(allEntities.next());
      nbEntitiesProcessed++;
      if (nbEntitiesProcessed % 1000 == 0) {
        LOGGER.info((nbEntitiesProcessed + " products processed."));
      }
    }

    closeExcelFile(nbEntitiesProcessed + 1);
  }

  void closeExcelFile(int numberOfRows) throws IOException {
    if (StringUtils.isNotBlank(getEntityType().getSimpleName())) {
      AreaReference reference =
          workbook
              .getXSSFWorkbook()
              .getCreationHelper()
              .createAreaReference(
                  new CellReference(0, 0),
                  new CellReference(numberOfRows - 1, getColumnsNames().length - 1));
      XSSFTable table = workbook.getXSSFWorkbook().getSheetAt(0).createTable(reference);
      CTTable cttable = table.getCTTable();

      cttable.setRef(reference.formatAsString());
      cttable.setDisplayName(getEntityType().getSimpleName());
      cttable.setName(getEntityType().getSimpleName());
      cttable.setId(1L);
      cttable.addNewTableStyleInfo();
      cttable.getTableStyleInfo().setName("TableStyleMedium8");
      cttable.getTableStyleInfo().setShowRowStripes(true);

      // cttable.setAutoFilter(CTAutoFilter.Factory.newInstance());
      table.getCTTable().addNewAutoFilter().setRef(table.getArea().formatAsString());

      for (int i = 0; i < getColumnsNames().length; i++) {
        // All columns have id 1, so we need repairing
        table.getCTTable().getTableColumns().getTableColumnArray(i).setId(i + 1);
      }
    }

    for (int i = 0; i < getColumnsNames().length; i++) {
      sheet.setColumnWidth(i, getColumnsNames()[i].length() * 450);
    }

    // Write the output to a file
    FileOutputStream fileOut = new FileOutputStream(getFileName());
    workbook.write(fileOut);
    fileOut.close();

    // Closing the workbook
    workbook.dispose();
    workbook.close();
  }
}
