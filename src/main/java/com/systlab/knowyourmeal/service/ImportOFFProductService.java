package com.systlab.knowyourmeal.service;

import com.systlab.knowyourmeal.dao.IngredientRepository;
import com.systlab.knowyourmeal.dao.ProductRepository;
import com.systlab.knowyourmeal.entity.Ingredient;
import com.systlab.knowyourmeal.entity.OFFIngredients;
import com.systlab.knowyourmeal.entity.OFFProducts;
import com.systlab.knowyourmeal.entity.Product;
import com.systlab.knowyourmeal.utils.RulesUtils;
import com.systlab.knowyourmeal.utils.TextUtils;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class ImportOFFProductService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ImportOFFProductService.class);

  private Map<String, Ingredient> ingredientMap;

  @Autowired private MongoTemplate mongoTemplate;

  @Autowired private ProductRepository productRepository;
  @Autowired private IngredientRepository ingredientRepository;

  public void importAllProducts() {
    ingredientMap = new LinkedHashMap<>(700000);

    CloseableIterator<OFFProducts> allProducts =
        mongoTemplate.stream(new Query(), OFFProducts.class);

    int nbProduitsTraites = 0;
    while (allProducts.hasNext()) {
      importProduct(allProducts.next());
      nbProduitsTraites++;
      if (nbProduitsTraites % 1000 == 0) {
        LOGGER.info(
            nbProduitsTraites
                + " processed products. "
                + ingredientMap.size()
                + " unique ingredients ("
                + String.format("%.2f", (float) ingredientMap.size() / (float) nbProduitsTraites)
                + " per product).");
      }
    }

    productRepository.saveRemainingBuffer();

    // Insert all ingredients at the end.
    ingredientRepository.insert(ingredientMap.values());
  }

  private void importProduct(OFFProducts OFFProducts) {
    List<String> productNames = OFFProducts.getValidNames();
    if (!CollectionUtils.isEmpty(productNames) && StringUtils.isNotEmpty(OFFProducts.getCode())) {
      Product product = new Product();
      product.setName(productNames.get(0));
      product.setCode(OFFProducts.getCode());
      product.setBrands(OFFProducts.getBrands());
      if (productNames.size() > 1) {
        productNames.remove(product.getName());
        product.setOtherNames(productNames);
      }
      if (OFFProducts.getIngredients() != null) {
        List<Ingredient> ingredients = mapAllIngredient(OFFProducts);
        product.setListIdIngredients(
            ingredients.stream().map(Ingredient::getId).distinct().collect(Collectors.toList()));
        product.setGluten(computeBoolean(ingredients, Ingredient::getGluten, false));
        product.setBeef(computeBoolean(ingredients, Ingredient::getBeef, false));
        product.setPork(computeBoolean(ingredients, Ingredient::getPork, false));
        product.setFish(computeBoolean(ingredients, Ingredient::getFish, false));
        product.setAlcohol(computeBoolean(ingredients, Ingredient::getAlcohol, false));

        if (Boolean.TRUE.equals(product.getBeef())
            || Boolean.TRUE.equals(product.getPork())
            || Boolean.TRUE.equals(product.getFish())) {
          product.setVegan(Boolean.FALSE);
        } else {
          product.setVegan(computeBoolean(ingredients, Ingredient::getVegan, true));
        }

        if (Boolean.TRUE.equals(product.getBeef()) || Boolean.TRUE.equals(product.getPork())) {
          product.setVegetarian(Boolean.FALSE);
        } else {
          product.setVegetarian(computeBoolean(ingredients, Ingredient::getVegetarian, true));
        }
      }
      productRepository.saveWithBuffer(product);
    }
  }

  private Boolean computeBoolean(
      List<Ingredient> ingredients,
      Function<Ingredient, Boolean> function,
      boolean trueForAllMatch) {
    if (CollectionUtils.isEmpty(ingredients)) {
      return null;
    }
    if (trueForAllMatch) {
      if (ingredients.stream().anyMatch(i -> function.apply(i) != null && !function.apply(i))) {
        return Boolean.FALSE;
      } else if (ingredients.stream()
          .allMatch(i -> function.apply(i) != null && function.apply(i))) {
        return Boolean.TRUE;
      }
    } else {
      if (ingredients.stream().anyMatch(i -> function.apply(i) != null && function.apply(i))) {
        return Boolean.TRUE;
      } else if (ingredients.stream()
          .allMatch(i -> function.apply(i) != null && !function.apply(i))) {
        return Boolean.FALSE;
      }
    }
    return null;
  }

  private List<Ingredient> mapAllIngredient(OFFProducts OFFProducts) {
    return OFFProducts.getIngredients().stream()
        .map(i -> mapIngredient(OFFProducts.getCode(), i))
        .filter(l -> !CollectionUtils.isEmpty(l))
        .flatMap(List::stream)
        .collect(Collectors.toList());
  }

  private List<Ingredient> mapIngredient(String productCode, OFFIngredients OFFIngredients) {
    String name = TextUtils.simplify(OFFIngredients.getText());

    if (StringUtils.isNotEmpty(name)) {
      Ingredient ingredient = ingredientMap.computeIfAbsent(name, k -> new Ingredient(name));

      ingredient.addToListOriginalNames(TextUtils.clean(OFFIngredients.getText()));
      ingredient.addToListProductsCodes(productCode);

      if ((ingredient.getVegan() == null || ingredient.getVegan())
          && StringUtils.isNotEmpty(OFFIngredients.getVegan())) {
        if (OFFIngredients.getVegan().toLowerCase().contains("yes")
            || OFFIngredients.getVegan().toLowerCase().contains("oui")) {
          ingredient.setVegan(Boolean.TRUE);
        }
        if (OFFIngredients.getVegan().toLowerCase().contains("no")) {
          ingredient.setVegan(Boolean.FALSE);
        }
      }
      if ((ingredient.getVegetarian() == null || ingredient.getVegetarian())
          && StringUtils.isNotEmpty(OFFIngredients.getVegetarian())) {
        if (OFFIngredients.getVegetarian().toLowerCase().contains("yes")
            || OFFIngredients.getVegetarian().toLowerCase().contains("oui")) {
          ingredient.setVegetarian(Boolean.TRUE);
        }
        if (OFFIngredients.getVegetarian().toLowerCase().contains("no")) {
          ingredient.setVegetarian(Boolean.FALSE);
        }
      }
      if (containsAnyWord(name, RulesUtils.getWordsContainingAlcohol())) {
        ingredient.setAlcohol(Boolean.TRUE);
      }
      if (containsAnyWord(name, RulesUtils.getWordsContainingBeef())) {
        ingredient.setBeef(Boolean.TRUE);
      }
      if (containsAnyWord(name, RulesUtils.getWordsContainingFish())) {
        ingredient.setFish(Boolean.TRUE);
      }
      if (containsAnyWord(name, RulesUtils.getWordsContainingGluten())) {
        ingredient.setGluten(Boolean.TRUE);
      }
      if (containsAnyWord(name, RulesUtils.getWordsContainingPork())) {
        ingredient.setPork(Boolean.TRUE);
      }
      return Collections.singletonList(ingredient);
    }
    return null;
  }

  private boolean containsAnyWord(String name, List<String> words) {
    return words.stream().anyMatch(w -> name.contains(w));
  }
}
