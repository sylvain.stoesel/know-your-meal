package com.systlab.knowyourmeal.service;

import com.systlab.knowyourmeal.entity.Ingredient;
import com.systlab.knowyourmeal.utils.ExportUtils;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Service;

@Service
public class ExportIngredientService extends AbstractExportExcelService<Ingredient> {

  private int rowNum;

  @Override
  Class<Ingredient> getEntityType() {
    return Ingredient.class;
  }

  @Override
  String getFileName() {
    return "Unique-Ingredients.xlsx";
  }

  @Override
  String[] getColumnsNames() {
    return new String[] {
      "ID DB",
      "Name saved in database",
      "# products",
      "Old name(s) to be ignored",
      "Delete?",
      "VEGAN",
      "VEGETARIAN",
      "GLUTEN",
      "BEEF",
      "PORK",
      "FISH",
      "ALCOHOL"
    };
  }

  @Override
  void createExcelFile() {
    super.createExcelFile();
    Cell cell = this.sheet.getRow(0).createCell(getColumnsNames().length);
    cell.setCellValue("AJOUTER ICI LE OU LES NOMS PAR LESQUELS CET ARTICLE DOIT ETRE REMPLACE");
    this.rowNum = 1;
  }

  @Override
  public void writeRowInExcelFile(Ingredient ingredient) {
    Row row = sheet.createRow(rowNum++);

    row.createCell(0).setCellValue(ingredient.getId());
    row.createCell(1).setCellValue(ingredient.getName());
    row.createCell(2).setCellValue(ingredient.getListProductsCodes().size());

    Cell cell = row.createCell(3);
    cell.setCellStyle(styleFontSmall);
    cell.setCellValue(
        ingredient.getListOriginalNames().stream()
            .filter(n -> !n.equals(ingredient.getName()))
            .collect(Collectors.joining(", ")));

    row.createCell(4).setCellValue("");
    row.createCell(5).setCellValue(ExportUtils.getTextForBoolean(ingredient.getVegan()));
    row.createCell(6).setCellValue(ExportUtils.getTextForBoolean(ingredient.getVegetarian()));
    row.createCell(7).setCellValue(ExportUtils.getTextForBoolean(ingredient.getGluten()));
    row.createCell(8).setCellValue(ExportUtils.getTextForBoolean(ingredient.getBeef()));
    row.createCell(9).setCellValue(ExportUtils.getTextForBoolean(ingredient.getPork()));
    row.createCell(10).setCellValue(ExportUtils.getTextForBoolean(ingredient.getFish()));
    row.createCell(11).setCellValue(ExportUtils.getTextForBoolean(ingredient.getAlcohol()));
  }
}
