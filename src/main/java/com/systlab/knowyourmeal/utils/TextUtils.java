package com.systlab.knowyourmeal.utils;

import java.text.Normalizer;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class TextUtils {

  private static final Map<String, List<String>> RULES_FOR_CLEANING =
      RulesUtils.getRulesForCleaning();
  private static final List<String> RULES_FOR_SIMPLIFYING =
      RulesUtils.getRegexToRemoveForSimplifying();

  private static final List<String> IGNORABLE_WORDS = RulesUtils.getIgnorableWords();

  public static String clean(String toBeCleaned) {
    if (StringUtils.isNotEmpty(toBeCleaned)) {
      String result = toBeCleaned.toLowerCase();
      for (Map.Entry<String, List<String>> entry : RULES_FOR_CLEANING.entrySet()) {
        for (String toBeReplaced : entry.getValue())
          result = result.replace(toBeReplaced, entry.getKey());
      }
      return result.trim().replaceAll(" +", " ");
    }
    return "";
  }

  public static String simplify(String toBeSimplified) {
    if (StringUtils.isNotEmpty(toBeSimplified)) {
      String result = stripAccents(clean(toBeSimplified));

      for (String regex : RULES_FOR_SIMPLIFYING) {
        result = result.replaceAll(regex, "");
      }
      result = result.replaceAll("[#,;&]", " ").replaceAll(" +", " ");

      if (isConsideredAsEmpty(result)) {
        return "";
      }
      return result;
    }
    return "";
  }

  private static boolean isConsideredAsEmpty(String text) {
    text = text.replaceAll("[0-9\\.]+", "").trim();
    return StringUtils.isBlank(text) || text.length() == 1 || IGNORABLE_WORDS.contains(text);
  }

  private static String stripAccents(String s) {
    s = Normalizer.normalize(s, Normalizer.Form.NFD);
    return s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "").replace("ß", "ss");
  }
}
