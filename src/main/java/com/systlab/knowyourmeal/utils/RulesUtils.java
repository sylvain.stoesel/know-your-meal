package com.systlab.knowyourmeal.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RulesUtils {

  private static List<String> wordsContainingAlcohol =
      readLinesOfFile("words_contains_alcohol.txt");
  private static List<String> wordsContainingBeef = readLinesOfFile("words_contains_beef.txt");
  private static List<String> wordsContainingFish = readLinesOfFile("words_contains_fish.txt");
  private static List<String> wordsContainingGluten = readLinesOfFile("words_contains_gluten.txt");
  private static List<String> wordsContainingPork = readLinesOfFile("words_contains_pork.txt");

  public static List<String> getWordsContainingAlcohol() {
    return wordsContainingAlcohol;
  }

  public static List<String> getWordsContainingBeef() {
    return wordsContainingBeef;
  }

  public static List<String> getWordsContainingFish() {
    return wordsContainingFish;
  }

  public static List<String> getWordsContainingGluten() {
    return wordsContainingGluten;
  }

  public static List<String> getWordsContainingPork() {
    return wordsContainingPork;
  }

  public static List<String> getRegexToRemoveForSimplifying() {
    return readLinesOfFile("simplify_remove_regex.txt");
  }

  public static List<String> getIgnorableWords() {
    return readLinesOfFile("ignorable_words.txt");
  }

  public static Map<String, List<String>> getRulesForCleaning() {
    return buildRulesMap(readLinesOfFile("clean_replace_chars.txt"));
  }

  private static Map<String, List<String>> buildRulesMap(List<String> fileLines) {
    Map<String, List<String>> result = new LinkedHashMap<>();

    for (String rule : fileLines) {
      String toBeReplaced, replaceBy;
      if (rule.length() >= 3 && rule.contains(":")) {
        replaceBy = rule.split(":")[0];
        toBeReplaced = rule.split(":")[1];
      } else {
        replaceBy = "";
        toBeReplaced = rule;
      }
      List<String> listOfToBeReplaced = result.computeIfAbsent(replaceBy, k -> new ArrayList<>());
      if (!listOfToBeReplaced.contains(toBeReplaced)) {
        listOfToBeReplaced.add(toBeReplaced);
      }
    }
    return result;
  }

  private static List<String> readLinesOfFile(String name) {
    try {
      BufferedReader reader =
          new BufferedReader(
              new InputStreamReader(
                  (RulesUtils.class.getClassLoader().getResourceAsStream("rules/" + name))));
      String line;
      List<String> result = new ArrayList<>();
      while ((line = reader.readLine()) != null) {
        result.add(line);
      }
      return result;
    } catch (Exception e) {
      return Collections.emptyList();
    }
  }
}
