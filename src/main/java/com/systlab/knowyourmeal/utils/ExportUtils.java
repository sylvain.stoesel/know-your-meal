package com.systlab.knowyourmeal.utils;

public class ExportUtils {

  public static String getTextForBoolean(Boolean b) {
    return b != null ? (b ? "YES" : "NO") : "";
  }
}
