package com.systlab.knowyourmeal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "Ingredient")
public class Ingredient {

  private static Long LAST_ID = 1L;

  @Id private Long id;

  @Indexed private String name;

  @Indexed private int nbProduitsContenu;

  @Indexed private Boolean vegan;
  @Indexed private Boolean vegetarian;
  @Indexed private Boolean gluten;
  @Indexed private Boolean beef;
  @Indexed private Boolean pork;
  @Indexed private Boolean fish;
  @Indexed private Boolean alcohol;

  private List<String> listOriginalNames = new ArrayList<>();

  private List<String> listProductsCodes = new ArrayList<>();

  public Ingredient(String name) {
    this.id = LAST_ID++;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNbProduitsContenu() {
    return nbProduitsContenu;
  }

  public void setNbProduitsContenu(int nbProduitsContenu) {
    this.nbProduitsContenu = nbProduitsContenu;
  }

  public List<String> getListProductsCodes() {
    return listProductsCodes;
  }

  public void setListProductsCodes(List<String> listProductsCodes) {
    this.listProductsCodes = listProductsCodes;
  }

  public Boolean getVegan() {
    return vegan;
  }

  public void setVegan(Boolean vegan) {
    this.vegan = vegan;
  }

  public Boolean getVegetarian() {
    return vegetarian;
  }

  public void setVegetarian(Boolean vegetarian) {
    this.vegetarian = vegetarian;
  }

  public Boolean getGluten() {
    return gluten;
  }

  public void setGluten(Boolean gluten) {
    this.gluten = gluten;
  }

  public Boolean getBeef() {
    return beef;
  }

  public void setBeef(Boolean beef) {
    this.beef = beef;
  }

  public Boolean getPork() {
    return pork;
  }

  public void setPork(Boolean pork) {
    this.pork = pork;
  }

  public Boolean getFish() {
    return fish;
  }

  public void setFish(Boolean fish) {
    this.fish = fish;
  }

  public Boolean getAlcohol() {
    return alcohol;
  }

  public void setAlcohol(Boolean alcohol) {
    this.alcohol = alcohol;
  }

  public void addToListProductsCodes(String codeProduit) {
    if (!this.listProductsCodes.contains(codeProduit)) {
      this.listProductsCodes.add(codeProduit);
    }
  }

  public List<String> getListOriginalNames() {
    return listOriginalNames;
  }

  public void setListOriginalNames(List<String> listOriginalNames) {
    this.listOriginalNames = listOriginalNames;
  }

  public void addToListOriginalNames(String originalName) {
    if (!this.listOriginalNames.contains(originalName)) {
      this.listOriginalNames.add(originalName);
    }
  }

  @Override
  public boolean equals(Object other) {
    return other instanceof Ingredient && ((Ingredient) other).id.equals(this.id);
  }
}
