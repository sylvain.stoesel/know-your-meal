package com.systlab.knowyourmeal.entity;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Product")
public class Product {

  private static Long LAST_ID = 1L;

  @Id private Long id;

  @Indexed private String code;

  @Indexed private String name;

  private List<String> otherNames;

  @Indexed private Boolean vegan;
  @Indexed private Boolean vegetarian;
  @Indexed private Boolean gluten;
  @Indexed private Boolean beef;
  @Indexed private Boolean pork;
  @Indexed private Boolean fish;
  @Indexed private Boolean alcohol;

  private List<String> brands;

  private List<Long> listIdIngredients;

  public Product() {
    this.id = LAST_ID++;
  }

  public Product(String code) {
    this.code = code;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getVegan() {
    return vegan;
  }

  public void setVegan(Boolean vegan) {
    this.vegan = vegan;
  }

  public Boolean getVegetarian() {
    return vegetarian;
  }

  public void setVegetarian(Boolean vegetarian) {
    this.vegetarian = vegetarian;
  }

  public Boolean getGluten() {
    return gluten;
  }

  public void setGluten(Boolean gluten) {
    this.gluten = gluten;
  }

  public Boolean getBeef() {
    return beef;
  }

  public void setBeef(Boolean beef) {
    this.beef = beef;
  }

  public Boolean getPork() {
    return pork;
  }

  public void setPork(Boolean pork) {
    this.pork = pork;
  }

  public Boolean getFish() {
    return fish;
  }

  public void setFish(Boolean fish) {
    this.fish = fish;
  }

  public Boolean getAlcohol() {
    return alcohol;
  }

  public void setAlcohol(Boolean alcohol) {
    this.alcohol = alcohol;
  }

  public List<String> getBrands() {
    return brands;
  }

  public void setBrands(List<String> brands) {
    this.brands = brands;
  }

  public List<Long> getListIdIngredients() {
    return listIdIngredients;
  }

  public void setListIdIngredients(List<Long> listIdIngredients) {
    this.listIdIngredients = listIdIngredients;
  }

  public List<String> getOtherNames() {
    return otherNames;
  }

  public void setOtherNames(List<String> otherNames) {
    this.otherNames = otherNames;
  }
}
