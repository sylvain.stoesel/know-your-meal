package com.systlab.knowyourmeal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "ingredients")
public class OFFIngredients {

  @Id private String id;

  private String text;

  private String vegan;

  private String vegetarian;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getVegan() {
    return vegan;
  }

  public void setVegan(String vegan) {
    this.vegan = vegan;
  }

  public String getVegetarian() {
    return vegetarian;
  }

  public void setVegetarian(String vegetarian) {
    this.vegetarian = vegetarian;
  }
}
