package com.systlab.knowyourmeal.entity;

import com.systlab.knowyourmeal.utils.TextUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products")
public class OFFProducts {

  @Id private String id;

  private String code;

  private String product_name;
  private String product_name_en;
  private String generic_name_fr;

  private List<String> brands;

  private List<String> ingredients_hierarchy;

  private List<OFFIngredients> ingredients;

  public OFFProducts() {}

  public OFFProducts(String code) {
    this.code = code;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public List<String> getIngredients_hierarchy() {
    return ingredients_hierarchy;
  }

  public void setIngredients_hierarchy(List<String> ingredients_hierarchy) {
    this.ingredients_hierarchy = ingredients_hierarchy;
  }

  public List<OFFIngredients> getIngredients() {
    return ingredients;
  }

  public void setIngredients(List<OFFIngredients> ingredients) {
    this.ingredients = ingredients;
  }

  public String getProduct_name_en() {
    return product_name_en;
  }

  public void setProduct_name_en(String product_name_en) {
    this.product_name_en = product_name_en;
  }

  public List<String> getBrands() {
    return brands;
  }

  public void setBrands(List<String> brands) {
    this.brands = brands;
  }

  public String getProduct_name() {
    return product_name;
  }

  public void setProduct_name(String product_name) {
    this.product_name = product_name;
  }

  public String getGeneric_name_fr() {
    return generic_name_fr;
  }

  public void setGeneric_name_fr(String generic_name_fr) {
    this.generic_name_fr = generic_name_fr;
  }

  public List<String> getValidNames() {
    List<String> resultat = new ArrayList<>();
    if (StringUtils.isNotBlank(TextUtils.clean(getProduct_name()))) {
      resultat.add(TextUtils.clean(getProduct_name()));
    }
    if (StringUtils.isNotBlank(getProduct_name_en()) && !resultat.contains(getProduct_name_en())) {
      resultat.add(TextUtils.clean(getProduct_name_en()));
    }
    if (StringUtils.isNotBlank(getGeneric_name_fr()) && !resultat.contains(getGeneric_name_fr())) {
      resultat.add(TextUtils.clean(getGeneric_name_fr()));
    }
    return resultat;
  }


  @Override
  public String toString() {
    return "OFFProducts{"
        + "id='"
        + id
        + '\''
        + ", code='"
        + code
        + '\''
        + ", product_name_en='"
        + product_name_en
        + "\'}";
  }
}
