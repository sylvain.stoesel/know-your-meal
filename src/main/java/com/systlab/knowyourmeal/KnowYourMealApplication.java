package com.systlab.knowyourmeal;

import com.systlab.knowyourmeal.service.ExportIngredientService;
import com.systlab.knowyourmeal.service.ExportProductService;
import com.systlab.knowyourmeal.service.ImportOFFProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnowYourMealApplication implements CommandLineRunner {

  @Autowired private ImportOFFProductService importOFFProductService;
  @Autowired private ExportIngredientService exportIngredientService;
  @Autowired private ExportProductService exportProductService;

  public static void main(String[] args) {
    SpringApplication.run(KnowYourMealApplication.class, args);
  }

  @Override
  public void run(String... arg0) throws Exception {
    importOFFProductService.importAllProducts();
    exportProductService.exportAllEntities();
    exportIngredientService.exportAllEntities();
  }
}
