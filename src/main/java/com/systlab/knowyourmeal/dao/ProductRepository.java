package com.systlab.knowyourmeal.dao;

import com.systlab.knowyourmeal.entity.Product;
import com.systlab.knowyourmeal.utils.MongoConstants;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {

  Product findByCode(String code);

  Product findByName(String name);

  List<Product> productList = new ArrayList<>();

  default void saveWithBuffer(Product product) {
    productList.add(product);
    if (productList.size() > MongoConstants.ENTITY_BUFFER_SIZE) {
      this.insert(productList);
      productList.clear();
    }
  }

  default void saveRemainingBuffer() {
    if (productList.size() > 0) {
      this.insert(productList);
      productList.clear();
    }
  }
}
