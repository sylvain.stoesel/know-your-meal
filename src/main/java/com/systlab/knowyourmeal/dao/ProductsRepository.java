package com.systlab.knowyourmeal.dao;

import com.systlab.knowyourmeal.entity.OFFProducts;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductsRepository extends MongoRepository<OFFProducts, String> {

  OFFProducts findByCode(String code);

}
