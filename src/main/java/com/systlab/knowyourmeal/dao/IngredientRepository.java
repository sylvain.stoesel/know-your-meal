package com.systlab.knowyourmeal.dao;

import com.systlab.knowyourmeal.entity.Ingredient;
import com.systlab.knowyourmeal.utils.MongoConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IngredientRepository extends MongoRepository<Ingredient, String> {

  Ingredient findByName(String name);

}
