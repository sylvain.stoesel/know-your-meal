# KnowYourMeal Project

This project is aimed to analyze, extract and automatically categorize food products using the [OpenFoodFacts database](https://world.openfoodfacts.org/).
![OpenFoodFacts logo](openfoodfacts-logo.png)

## Prerequisites
To make this project work, you will need:
 - Windows 10 or a recent Linux distrib
 - [a MongoDB 4.x local server](https://www.mongodb.com/try/download/community), running as a service
 - [MongoDB database tools](https://www.mongodb.com/try/download/database-tools) to use the *mongorestore* command.
 - having `mongo` and `mongorestore` commands working.
   - On Windows, add MongoDb folder to your environment variable **Path**, i.e. `C:\Program Files\MongoDB\Server\4.x\bin`

## Launch import

 - On Windows, execute the PowerShell script: `import-products-windows.ps1`
 - On Linux, execute the Shell script: `import-products-linux.sh`

The script will :
 - compile the Java project and create a standalone jar **know-your-meal-x.x.x-SNAPSHOT.jar**
 - download the database dump from [The OpenFoodFacts website](https://world.openfoodfacts.org/data)
 - extract the dump and import it into the local MongoDb server
 - launch the standalone jar that will:
    - analyze all products from OpenFoodFacts and save them into the database
    - extract unique ingredients from each product and save them into the database
    - generate Excel files with unique Products and unique Ingredients