#!/bin/sh

gradlew clean build
cd mongo-import/
wget https://static.openfoodfacts.org/data/openfoodfacts-mongodbdump.tar.gz
7z x openfoodfacts-mongodbdump.tar.gz
rm openfoodfacts-mongodbdump.tar.gz
7z x openfoodfacts-mongodbdump.tar
rm openfoodfacts-mongodbdump.tar
mongo --eval "db.dropDatabase()" knowyourmeal
mongorestore -d knowyourmeal -c products dump/off/products.bson
rm -r dump/
java -Xms512m -Xmx1536m -jar build/libs/know-your-meal-1.0.0-SNAPSHOT.jar
